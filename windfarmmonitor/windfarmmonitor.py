import os
import numpy as np
import pandas as pd

import astropy as apy
from astropy import coordinates
from astropy import units as u
from astropy.coordinates import SkyCoord, AltAz, EarthLocation
from astropy.time import Time

import lofarobsxml as lox
from lofarantpos.db import LofarAntennaDatabase
from lofarantpos import geo

BM_DIRECT='direct'
BM_SPLIT='split'
BM_OMNI='omni'
BM_BANANA='banana'
BM_CELESTIAL='celestial'

RCU_10_90   = 1
RCU_110_190 = 5
RCU_170_230 = 6
RCU_210_250 = 7

DB                = LofarAntennaDatabase()
PQR_TO_ETRS       = DB.pqr_to_etrs['CS002LBA']
LOFAR_ORIGIN_ETRS = DB.phase_centres['CS002LBA']
LOFAR             = EarthLocation.from_geocentric(*LOFAR_ORIGIN_ETRS, unit='m')


# Amount of time that black ops script 'bo-job.sh' needs to start all
# beams. Station observation should start "LEAD_TIME" ahead of the
# correlator.
LEAD_TIME = {BM_DIRECT   : 1.5*u.min, 
             BM_OMNI     : 4*u.min,
             BM_SPLIT    : 1.5*u.min,
             BM_CELESTIAL: 1*u.min}

def wtg_base_etrs(dataframe, wtg_name):
    row = dataframe.loc[dataframe['NAME'] == wtg_name]
    return np.array([float(row[col])
                     for col in ['ETRS_X', 'ETRS_Y', 'ETRS_Z']])

def etrs_dir(field_etrs, target_etrs):
    dx, dy, dz = target_etrs-field_etrs
    return coordinates.SkyCoord(x=dx, y=dy, z=dz,
                                representation_type='cartesian',
                                frame='itrs')

def azel_dir(field_etrs, target_etrs):
    r'''
    Astropy AltAz corrresponds to casacore "AZELGEO"
    '''
    x,y,z = field_etrs
    return etrs_dir(field_etrs, target_etrs).transform_to(
            coordinates.AltAz(location=coordinates.EarthLocation(x, y, z, unit='m')))




def beamsetup_field(field_name, band, rcu_mode, main_subbands, cal_subbands, beam_config, field_etrs, ref_source_etrs, wtg_ref_etrs):
    antennaset = 'LBA_OUTER'
    rcus = '0:95'
    if 'LBA' not in band:
        hba_suffix={'0': '_ZERO', '1': '_ONE'}
        antennaset = 'HBA'+hba_suffix[field_name[-1]]
    azel_cal = azel_dir(field_etrs, ref_source_etrs)
    azel_wtg = azel_dir(field_etrs, wtg_ref_etrs)
    
    if beam_config in ['direct', 'omni']:
        #subbands='12:499'
        beamlets='0:487'
        if 'HBA0' in field_name:
            rcus = '0:47'
        elif 'HBA1' in field_name:
            rcus = '48:95'
            beamlets = '1000:1487'
        else: #LBA
            pass
        if 'direct' == beam_config:
            digdir = '%.7f,%.7f,AZELGEO' % (azel_cal.az.rad, azel_cal.alt.rad)
            return [(field_name, antennaset, main_subbands, beamlets, rcus, rcu_mode, digdir, digdir)]
        else:
            digdir = '0.0,0.0,RADIAL'
            if 'HBA1' in field_name:
                return [None]
            else:
                return [(field_name, antennaset, main_subbands, beamlets, rcus, rcu_mode, digdir, digdir)]
    elif beam_config == 'split':
        main_start, main_end = [int(sb) for sb in main_subbands.split(':')]
        main_beamlets = '0:%d' % (main_end - main_start)
        cal_beamlets = '%d:%d' % (main_end - main_start+1, main_end - main_start+1+len(cal_subbands.split(','))-1,)
        if 'HBA0' in field_name:
            rcus = '0:47'
        elif 'HBA1' in field_name:
            rcus = '48:95'
            main_beamlets = '1000:%d' % (1000+main_end - main_start)
            cal_beamlets = '%d:%d' % (1000+main_end - main_start+1, 1000+main_end - main_start +1 + len(cal_subbands.split(','))-1,)
        digdir_cal = '%.7f,%.7f,AZELGEO' % (azel_cal.az.rad, azel_cal.alt.rad)
        digdir_wtg = '%.7f,%.7f,AZELGEO' % (azel_wtg.az.rad, azel_wtg.alt.rad)
        return [(field_name, antennaset, main_subbands, main_beamlets, rcus, rcu_mode, digdir_wtg, digdir_wtg),
                (field_name, antennaset, cal_subbands, cal_beamlets, rcus, rcu_mode, digdir_cal, digdir_wtg)]
    else: #BM_CELESTIAL
        return [None]
    return NotImplementedError('%s mode not yet implemented.' % beam_config)

    
def beamsetup_all(obs_start_utc, obs_end_utc,
                  ref_source_etrs, wtg_ref_etrs,
                  rcu_mode, band, beam_config=BM_DIRECT,
                  main_subbands='12:499', cal_subbands=None,
                  sample_time=0.25, remarks='',
                  bit_mode=8, beam_start_interval_s=0.3,
                  db=DB):
    """
    band: 'HBA' or 'LBA'
    antennaset = 'LBA_OUTER'
    """
    clock_MHz = 200
    if rcu_mode == 6:
        clock_MHz = 160

    header = (obs_start_utc.iso, obs_end_utc.iso, bit_mode, beam_start_interval_s, clock_MHz)
    fields = [name for name in db.phase_centres.keys() if 'CS' in name and band in name]
    if 'HBA' == band:
        fields = [name for name in fields if name[-4:] in ['HBA0', 'HBA1']]
    targets = [row 
               for name in fields
               for row in beamsetup_field(name, band, rcu_mode, main_subbands, cal_subbands, beam_config, db.phase_centres[name],
                                          ref_source_etrs, wtg_ref_etrs)
               ]
    targets = [row for row in targets if row is not None]
    # FIELD ANTENNA_SET SUBBANDS BEAMLETS RCUS RCUMODE DIGDIR ANADIR
    return [header]+targets






def celestial_sources_alt_az(start_date_utc):
    tau_a = SkyCoord.from_name('Tau A')
    vir_a = SkyCoord.from_name('Vir A')
    c123 = SkyCoord.from_name('3C 123')
    tau_a, vir_a, LOFAR.get_gcrs(obstime=Time(start_date_utc, scale='utc'))
    dates = [Time('2019-09-02 19:00', scale='utc') + i*10*u.minute for i in range(6*10)]
    altaz_frames = AltAz(location=LOFAR, obstime=dates)
    # Cyg a 20 kJy
    # Cas A 20 kJy
    # Per A 400 Jy
    tau_a_pos = tau_a.transform_to(altaz_frames) # 2000 Jy: Demix Tau a from all Mid+high runs?
    vir_a_pos = vir_a.transform_to(altaz_frames) # 3000 Jy
    c123_pos  = c123.transform_to(altaz_frames)  # 450 Jy
    return pd.DataFrame(zip(pd.Series(Time(np.array(dates))),
                            pd.Series(tau_a_pos.alt), pd.Series(tau_a_pos.az),
                            pd.Series(vir_a_pos.alt), pd.Series(vir_a_pos.az),
                            pd.Series(c123_pos.alt), pd.Series(c123_pos.az),
                            ))



# 78 calibration subbands.
def cal_subbands(clock_MHz, sb_min=12, sb_max=499, max_cal_sb=78):
    subbands_raw = 1024*np.arange(0, clock_MHz//2)/clock_MHz
    subbands_rounded = np.array(np.around(subbands_raw), dtype=int)
    subbands_remainder = np.fabs(subbands_raw- subbands_rounded)
    selection = np.logical_and(np.logical_and(subbands_rounded >=sb_min, subbands_rounded <= sb_max),
                            subbands_remainder <= 0.5-(1.0/16.0))
    while selection.sum() > max_cal_sb:
        mx = subbands_remainder[selection].argmax()
        #print(subbands_remainder[selection][mx])
        new_selection = np.ones_like(selection[selection], dtype=bool)
        new_selection[mx] = False
        selection[selection] = new_selection
    #print(selection.sum())    
    return ','.join([str(n) for n in subbands_rounded[selection]])


def add_sidereal_days(date_str, number_of_days):
    return (Time(date_str)+number_of_days*u.sday).iso


def station_filename(start_date_utc, output_dir, suffix):
    return os.path.join(output_dir, start_date_utc.iso[:-7].replace(':', '').replace(' ', '_')+'-'+suffix+'.txt')


############################################################3


dmo = pd.read_csv('../share/wind-farm-monitor/drentse-monden-coordinates-RD-ETRS.csv')
dee_2_1_base_etrs      = wtg_base_etrs(dmo, 'DEE-2.1')
DEE_2_1_REFHEIGHT_ETRS = dee_2_1_base_etrs + np.dot(PQR_TO_ETRS, np.array([0, 0, 100]))
dee_2_1_nacelle_etrs   = dee_2_1_base_etrs + np.dot(PQR_TO_ETRS, np.array([0, 0, 145]))
dee_2_1_tip_etrs       = dee_2_1_base_etrs + np.dot(PQR_TO_ETRS, np.array([0, 0, 210]))
transmitter_V_etrs = np.array([3824410.12872055, 464680.28353332, 5066312.90759806])
transmitter_H_etrs = np.array([3824420.67706747, 464695.25686565, 5066303.63406967])
dmo[dmo['NAME'] == 'DEE-2.1']

####testcase
np.dot(PQR_TO_ETRS.T, transmitter_H_etrs - LOFAR_ORIGIN_ETRS)
####=>array([3904.26108017, 2208.64628792,   99.02721574])

####testcase
cal_subbands(200,  51, 460,  78)
####=>'51,56,72,77,82,87,92,97,102,113,118,123,128,133,138,143,154,159,164,169,174,179,184,200,205,210,215,220,225,230,241,246,251,256,261,266,271,282,287,292,297,302,307,312,328,333,338,343,348,353,358,369,374,379,384,389,394,399,410,415,420,425,430,435,440,456'






###############################################

def generate_observation_files(schedule_df, output_directory, transmitter_etrs, target_etrs=DEE_2_1_REFHEIGHT_ETRS,
                                project_name='WTG Monitoring',
                                folder_name='WTG Monitoring', folder_description='Monitoring potential DMO interference'):
    r'''
    schedule_df = Pandas DataFrame
    '''
    mom_observations = []
    backend_LBA = lox.BackendProcessing(channels_per_subband=64,
                                        default_template='BeamObservationNoStationControl',
                                        integration_time_seconds=1.0)
    backend_HBA = lox.BackendProcessing(channels_per_subband=64,
                                        default_template='BeamObservationNoStationControl',
                                        integration_time_seconds=2.0)
    first_start_date = None
    for ix, row in schedule_df.iterrows():
        station_start = Time(row[('Start', '(UTC)')], scale='utc') - LEAD_TIME[row[('Beam','')]]
        if first_start_date is None:
            first_start_date = Time(row[('Start', '(UTC)')], scale='utc')
        band = row[('Band', '')]
        sample_time = 1.0
        backend = backend_LBA
        if 'HBA' in band.upper():
            sample_time = 2.0
            backend = backend_HBA
        obs_rows = beamsetup_all(obs_start_utc=station_start,
                                 obs_end_utc=Time(row[('End', '(UTC)')], scale='utc'),
                                 ref_source_etrs=transmitter_etrs[row[('TX pol', '(H/V)')]],
                                 wtg_ref_etrs=target_etrs,
                                 rcu_mode=row[('RCU', '')],
                                 band=band,
                                 beam_config=row[('Beam', '')],
                                 main_subbands=row[('Subbands', '(main)')],
                                 cal_subbands=row[('Subbands', '(aux)')],
                                 sample_time=sample_time, remarks='',
                                 bit_mode=8, beam_start_interval_s=0.3,
                                 db=DB)
        with open(station_filename(Time(row[('Start', '(UTC)')], scale='utc'), 
                                   output_dir=output_directory,
                                   suffix='sept-%s-%s' % (row[('Band', '')], row[('Beam', '')])), 'w') as out_txt:
            out_txt.write('   '.join([str(n)[:19] for n in obs_rows[0]])+'\n')
            out_txt.write('\n'.join('%9s    %12s    %8s    %12s    %8s    %d   %s    %s' % row for row in obs_rows[1:]))

        duration = Time(row[('End', '(UTC)')], scale='utc') - Time(row[('Start', '(UTC)')], scale='utc')
        beam_list = []
        if row[('Beam','')] in [BM_DIRECT, BM_OMNI]:
            beam_list = [lox.Beam(sap_id=0,
                                  target_source=lox.TargetSource(name='REF_SRC', ra_angle=lox.Angle(deg=0.0), dec_angle=lox.Angle(deg=+90.0), reference_frame='AZELGEO'),
                                  subband_spec=row[('Subbands', '(main)')].replace(':', '..'))]
        elif row[('Beam', '')] == BM_SPLIT:
            beam_list = [lox.Beam(sap_id=0,
                                  target_source=lox.TargetSource(name='WTG', ra_angle=lox.Angle(deg=0.0), dec_angle=lox.Angle(deg=+90.0), reference_frame='AZELGEO'),
                                  subband_spec=row[('Subbands', '(main)')].replace(':', '..')),
                         lox.Beam(sap_id=1,
                                  target_source=lox.TargetSource(name='REF_SRC', ra_angle=lox.Angle(deg=0.0), dec_angle=lox.Angle(deg=+90.0), reference_frame='AZELGEO'),
                                  subband_spec=row[('Subbands', '(aux)')].replace(':', '..'))]

        antenna_set = 'HBA_DUAL'
        if row[('Band', '')] == 'LBA':
            antenna_set = 'LBA_OUTER'
        mom_observations.append(
            lox.Observation(name=row[('Band', '')]+'-'+row[('Beam','')]+'-'+str(row[('RCU', '')])+' '+str(row[('TX rate', '(kHz)')])+' kHz; '+str(row[('TX power', '(dB)')])+' dB; '+row[('TX pol', '(H/V)')],
                            start_date=Time(row[('Start', '(UTC)')], scale='utc').datetime.timetuple()[:6],
                            duration_seconds=duration.to(u.s).value,
                            stations=lox.station_list('core'),
                            antenna_set=antenna_set,
                            frequency_range={RCU_10_90: 'LBA_LOW', RCU_110_190: 'HBA_LOW', RCU_170_230: 'HBA_MID', RCU_210_250: 'HBA_HIGH'}[row[('RCU', '')]],
                            clock_mhz=obs_rows[0][4],
                            bit_mode=obs_rows[0][2],
                            beam_list=beam_list,
                            initial_status='approved',
                            backend=backend))
        print(duration)
        pass
    folder = lox.Folder(name=folder_name, children=mom_observations, grouping_parent=True, description=folder_description)
    with open(os.path.join(output_directory, first_start_date.iso[:-7].replace(':', '').replace(' ', '_')+'-wtg-monitoring.xml'), 'w') as out:
        out.write(lox.xml([folder], project=project_name, description='Data related to wind turbine monitoring experiments '))

        
